#include <iostream>
#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
#if defined(NDEBUG)
	std::cout << "Release build" << std::endl;
#else
  	std::cout << "Debug build" << std::endl;
#endif

	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();
}
