file(GLOB_RECURSE Sources RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.cpp)
file(GLOB_RECURSE Headers RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.h)
file(GLOB_RECURSE Uis RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} *.ui)

qt5_wrap_ui(Uicpps ${Uis})

add_executable(main ${Sources} ${Headers} ${Uicpps})

target_link_libraries(main Qt5::Core Qt5::Widgets)

add_test(main main)

install(TARGETS main
	RUNTIME
	DESTINATION .
	COMPONENT applications)


if(WIN32)
	include(WinDeployQt)
	WinDeployQt(TARGET main COMPILER_RUNTIME INCLUDE_MODULES ${QTLIBS} EXCLUDE_MODULES webkit webkit2)
	install(DIRECTORY ${PROJECT_BINARY_DIR}/windeployqt/
		DESTINATION .)
endif()
